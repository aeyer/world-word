﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordListImage : MonoBehaviour
{

    private List<ScrollerText> EmptyTextList = new List<ScrollerText>();
    private List<string[]> stringList = new List<string[]>();
    private Vector2 startPosition = new Vector2(-25f, -35f);
    private int listTrueLength;
    private Vector2 offset = new Vector2(20f, 7f);
    private List<string> textList = new List<string>();
    public static Func<List<string[]>> SpawnSetStr;
    public GameObject textShow;
    private string str;

    public ScrollerText scrollerText;
    public GameObject scrollerImage;
    
    
    void Start()
    {
        SpawnPosition();
        SetStartPosition();
    }

    public void SpawnPosition()
    {
        stringList = SpawnSetStr();

        for (int i = stringList.Count; i > 0; i--)
        {
            var text = Instantiate(scrollerText, new Vector3(0f, 0f, 0f), Quaternion.identity, scrollerImage.transform);
            EmptyTextList.Add(text);
        }

    }

    public void SetStartPosition()
    {
        int textPlaceCount;
        if (stringList.Count % 2 == 0)
            textPlaceCount = stringList.Count / 2;
        else
            textPlaceCount = stringList.Count / 2 + 1;
        
        for(int i = 0 ; i<textPlaceCount ; i++)
        for (int j = 0; j < 2; j++)
        {
            if (i * 2 + j < stringList.Count)
            {
                EmptyTextList[i * 2 + j].GetComponent<RectTransform>().position =
                    new Vector3(startPosition.x + i * (offset.x), startPosition.y - j * (offset.y));
            }
        }

        listTrueLength = stringList.Count;
        str=listTrueLength.ToString();
        textShow.GetComponent<Text>().text = str + "/" + str;
        

        SetWordsOnTexts();

        GameManager.SelectIndex = ChoicenWordAnimation;
    }

    public void SetWordsOnTexts()
    {
        for (int i = 0; i < stringList.Count; i++)
        {
            string result = string.Concat(stringList[i]);
            EmptyTextList[i].GetComponent<Text>().text = Fa.faConvert(result);
        }
    }


    public bool ChoicenWordAnimation(int b)
    {
        EmptyTextList[b].GetComponent<Text>().text = null;
        EmptyTextList.RemoveAt(b);
        string str1 = stringList.Count.ToString();

        textShow.GetComponent<Text>().text = str1 + "/" + str;
        
        return true;
    }


}
