﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public void ActiveObject(GameObject pauseIcon)
    {
        pauseIcon.SetActive(true);
    }

    public void LoadScene(string sceneName )
    {
        SceneManager.LoadScene( sceneName ); 
    }

    public void DeActiveObject(GameObject pauseIcon)
    {
        pauseIcon.SetActive(false);
    }
    
}
