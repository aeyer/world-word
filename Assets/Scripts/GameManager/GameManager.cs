﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public Puzzel puzzelPrefab;
    public GameObject ImageCanvas;
    public GameObject SelectWordImage;
    private LineRenderer line;
    private int CountCompareTime;
    private int slope;
    private int swapColor;
    private int CountOfIstart;
    public static Func<int, bool> SelectIndex;
    private List<Vector2> StartPositionList = new List<Vector2>
    {
        new Vector2(-30f,30f),
        new Vector2(-30f,31f),
        new Vector2(-30f,31f)
    };
    private List<Vector2> offsetList = new List<Vector2>
    {
        new Vector2(8.2f,8.2f),
        new Vector2(6f,6f),
        new Vector2(5f,5f)
    };
    private List<int> matrixLengthList = new List<int>
    {
        63,121,168
    };
    private List<Vector2> lineSizeList = new List<Vector2>
    {
        new Vector2(5.5f,5.5f),
        new Vector2(4.5f,4.5f),
        new Vector2(4f,4f)
    };

    private List<Puzzel> puzzelList = new List<Puzzel>();

    private List<string[]> stringList = new List<string[]>();

    private Vector2 startPosition;
    private Vector2 offset;
    private Vector2 EndPosition;
    private List<string> selectWord= new List<string>();
    public static Func<int> SetNumber;
    private int number;


    private Camera camera;
    private float CameraTransform;
    private int matrixLength ;
    private int iStart, jStart, iFinal, jFinal;
    private int currLine;
    public Material[] myMaterials = new Material[4];

    
    private string[] persianAlphabet =
    {
        "ا", "ب", "پ", "ت", "ث", "ج", "چ", "ح", "خ", "د", "ذ", "ر", "ز", "ژ", "س", "ش", "ص", "ض", "ط", "ظ", "ع", "غ",
        "ف", "ق", "ک", "گ", "ل", "م", "ن", "و", "ه", "ی"
    };

    void Start()
    {
        // we didnt word on Prefab Scale what why we always panic,
        number = SetNumber();
        SpawnPosition(number);
    }
    
    private void SpawnPosition(int a)
    {
        matrixLength = matrixLengthList[a];
        for (int i = 0; i <= matrixLength; i++)
        {
            var puzzle = Instantiate(puzzelPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity, /*parent*/
                ImageCanvas.transform);
            puzzelList.Add(puzzle);

            puzzle.SelectCells = PickingPuzzleBetween;
            SetText.SelectList = SetStartPosition;
            startPosition = StartPositionList[a];
            offset = offsetList[a];
        }
    }


    private int floorSqrt(int x)
    {
        if (x == 0 || x == 1)
            return x;

        int i = 1, result = 1;
        while (result <= x)
        {
            i++;
            result = i * i;
        }

        return i - 1;
    }


    private bool SetStartPosition(List<string[]> StrList,int num)
    {
        stringList = StrList;
        WordListImage.SpawnSetStr = SendStringList;
        
        int sqr = floorSqrt(matrixLength + 1);
        int successfullyPlaceWord = 0;

        createEmptypuzzle(sqr);

        

        
        for (int lenghtOfList = stringList.Count ; lenghtOfList > 0; lenghtOfList--)
        {
            int length = stringList[lenghtOfList - 1].Length;
            string[] word = stringList[lenghtOfList - 1];
            successfullyPlaceWord = 0;


            while (successfullyPlaceWord == 0)
            {
                successfullyPlaceWord = ChoiceRandomPosition(length, sqr, word);
            }
        }

        FillWhitePlaces(sqr);
        
        return true;
    }

    public List<string[]> SendStringList()
    {
        return stringList;
    }
    
    private bool PickingPuzzleBetween(Vector2 finallPos, Vector2 startPos)
    {

        int sqr = floorSqrt(matrixLength + 1);

        for(int i =0 ; i< sqr ; i++)
            for (int j = 0; j < sqr; j++)
            {
                if (puzzelList[i * sqr + j].transform.position.x == startPos.x &&
                     puzzelList[i * sqr + j].transform.position.y == startPos.y)
                {
                    iStart = i;
                    jStart = j;
                }
            }
        
        
        FindFinalIndex(finallPos, startPos);
        
        
        if (GameObject.Find("Line1") != null)
        {
            Destroy(GameObject.Find("Line1"));
        }

        
        createLine(finallPos,startPos,"Line1",false);

        bool IsTrueCheck=LetsFindWordBetweenIndex(iStart, jStart, iFinal, jFinal);

        if (IsTrueCheck)
        {
            Destroy(GameObject.Find("Line1"));
            createLine(finallPos,startPos,"Line2",true);
        }
        
        return true;
    }

    public bool LetsFindWordBetweenIndex(int iStart, int jStart, int iFinal, int jFinal)
    {
        selectWord.Clear();
        
 
       if (iStart == iFinal)
       {
           FindLetters(iStart, jStart, iFinal, jFinal,1);
       }else if (jStart == jFinal)
       {
           FindLetters(jStart,iStart,jFinal,iFinal,2);
       }else
       {
                slope = (iFinal - iStart) / (jFinal - jStart);
                CountOfIstart = iStart;
                while (CountOfIstart != iFinal )
                {
                    SetLettersToSelectWord(CountOfIstart, slope*(CountOfIstart-iStart)+jStart);
                    
                    if (iStart > iFinal)
                        CountOfIstart--;
                    else
                        CountOfIstart++;

                    if (CountOfIstart == iFinal)
                        SetLettersToSelectWord(CountOfIstart, slope * (CountOfIstart - iStart) + jStart);
                    
                }
        }

        bool IsWordTrue = CheckWithList(selectWord);

        if (IsWordTrue)
        {
            return true;
        }

        return false;
    }

    private void FindLetters(int iStart, int jStart, int iFinal, int jFinal ,int IsRightSide)
    {
        if (jStart > jFinal)
        {
            int deffrent = jFinal - jStart;

            for (int i = jStart; i >= jStart + deffrent; i--)
            {
                if (IsRightSide==1)
                    selectWord = SetLettersToSelectWord(iStart, i);
                else if (IsRightSide == 2)
                {
                    selectWord = SetLettersToSelectWord(i, iStart);
                }
            }
        }
        else
        {
            int deffrent = jFinal - jStart;

            for (int i = jStart; i <= jStart + deffrent; i++)
            {
                if (IsRightSide == 1)
                    selectWord = SetLettersToSelectWord(iStart, i);
                else if (IsRightSide == 2)
                {
                    selectWord = SetLettersToSelectWord(i, iStart);
                }
            }
        }
    }

    public List<string> SetLettersToSelectWord(int iStart, int i  )
    {
        int sqr = floorSqrt(matrixLength + 1);
        selectWord.Add(puzzelList[iStart * sqr + i].GetComponentInChildren<Text>().text);
        return selectWord;
    }


    public bool CheckWithList(List<string> selectWord)
    {
        for (int  k = stringList.Count ;  k>0  ;k-- )
        {
            string[] word = stringList[k - 1];
            string[] word1 = selectWord.ToArray();

            string result = String.Concat(word1);
            SelectWordImage.GetComponentInChildren<Text>().text = Fa.faConvert(result);

            if (word.Length == word1.Length)
            {
                CountCompareTime = 0;
                int i = word.Length;
                while (word[i-1] == word1[i-1])
                {
                    CountCompareTime++;
                    i--;

                    if (i == 0)
                    {
                        break;
                    }
                }

                if (word.Length == CountCompareTime)
                {
                    stringList.RemoveAt(k-1);
                    SelectIndex(k-1);
                    return true;
                }
            }
        }

        return false;
    }
    
    
    public void createLine(Vector2 finallPos , Vector2 startPos, string a,bool Check)
    {
        line = new GameObject(a).AddComponent<LineRenderer>();
        line.material = myMaterials[swapColor];
        line.positionCount = 2;
        line.startWidth =lineSizeList[2].x;
        line.endWidth = lineSizeList[2].y;
        line.useWorldSpace = true;
        line.numCapVertices = 50;
        line.sortingOrder = 50;
        line.SetPosition(0 , startPos);
        line.SetPosition(1 , finallPos);
        
        if (Check)
            swapColor++;
        
        if (myMaterials.Length == swapColor)
            swapColor = 0;
    }

    private void FindFinalIndex(Vector2 finallPos, Vector2 startPos)
    {

        float indexX = Mathf.Abs( finallPos.x - startPos.x) / offset.x;
        float indexY = Mathf.Abs( finallPos.y -  startPos.y) / offset.y;

        
        indexY = Mathf.Round(indexY);
        indexX = Mathf.Round(indexX);
        
        
        if (finallPos.x > startPos.x)
            jFinal = jStart + (int)indexX;
        else
            jFinal = jStart - (int)indexX;


        if (finallPos.y < startPos.y)
            iFinal = iStart + (int)indexY;
        else
            iFinal = iStart - (int)indexY;

    }
    
    private int ChoiceRandomPosition(int length, int sqr, string[] word)
    {
        int checkOutput;
        do
        {
            int iTarget = Random.Range(0, sqr );
            int jTarget = Random.Range(0, sqr );
            checkOutput = CheckEmptyOrWhat(iTarget, jTarget, sqr, word, length);
        } while (checkOutput == 0);

        return 1;
    }


    private void createEmptypuzzle(int sqr)
    {
        for (int i = 0; i < sqr; i++)
            for (int j = 0; j < sqr; j++)
                puzzelList[i * sqr + j].GetComponent<RectTransform>().position=
                    new Vector3(startPosition.x + j * offset.x, startPosition.y - i * offset.y, 10f);
    }


    private int CheckEmptyOrWhat(int iTarget, int jTarget, int sqr, string[] word, int length)
    {
        int checkOutput = 0;

        if (puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text == ""
            || puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text == word[0])
        {
            checkOutput = ChoiceSide(iTarget, jTarget, sqr, word, length);
        }

        return checkOutput;
    }


    private int ChoiceSide(int iTarget, int jTarget, int sqr, string[] word, int length)
    {
        int iMax = sqr - 1, jMax = sqr - 1;
        int checkTrue = 0;
        int checkOutput = 0;
        
        if (iTarget >= length - 1 && jMax - jTarget + 1 >= length && checkTrue == 0) //top  - right
        {
            checkTrue = TopRightSide(iTarget, jTarget, sqr, word, length, ref checkOutput);
        }
        
        if (iMax - iTarget + 1 >= length && jMax - jTarget + 1 >= length && checkTrue == 0) //down - right
        {
            checkTrue = DownRightSide(iTarget, jTarget, sqr, word, length, ref checkOutput);
        }
        
        if (jTarget >= length - 1 && checkTrue == 0) //left
        {
            checkTrue = LeftSide(iTarget, jTarget, sqr, word, length, ref checkOutput);
        }

        if (jMax - jTarget + 1 >= length && checkTrue == 0) //right
        {
            checkTrue = RightSide(iTarget, jTarget, sqr, word, length, ref checkOutput);
        }

        if (iTarget >= length - 1 && checkTrue == 0) //up
        {
            checkTrue = UpSide(iTarget, jTarget, sqr, word, length, ref checkOutput);
        }

        if (iMax - iTarget + 1 >= length && checkTrue == 0) //down 
        {
            checkOutput = DownSide(iTarget, jTarget, sqr, word, length, checkOutput);
        }

        return checkOutput;
    }

    private int DownSide(int iTarget, int jTarget, int sqr, string[] word, int length, int checkOutput)
    {
        int checkTrue;
        checkTrue = CheckEmptyOrWhat2(iTarget + 1, jTarget, sqr, word, length, 2);
        if (checkTrue == 2)
        {
            PutLetter(word, length, checkTrue, iTarget, jTarget);
            checkOutput++;
        }

        return checkOutput;
    }

    private int UpSide(int iTarget, int jTarget, int sqr, string[] word, int length, ref int checkOutput)
    {
        int checkTrue;
        checkTrue = CheckEmptyOrWhat2(iTarget - 1, jTarget, sqr, word, length, 1);
        if (checkTrue == 1)
        {
            PutLetter(word, length, checkTrue, iTarget, jTarget);
            checkOutput++;
        }

        return checkTrue;
    }

    private int RightSide(int iTarget, int jTarget, int sqr, string[] word, int length, ref int checkOutput)
    {
        int checkTrue;
        checkTrue = CheckEmptyOrWhat2(iTarget, jTarget + 1, sqr, word, length, 4);
        if (checkTrue == 4)
        {
            PutLetter(word, length, checkTrue, iTarget, jTarget);
            checkOutput++;
        }

        return checkTrue;
    }

    private int LeftSide(int iTarget, int jTarget, int sqr, string[] word, int length, ref int checkOutput)
    {
        int checkTrue;
        checkTrue = CheckEmptyOrWhat2(iTarget, jTarget - 1, sqr, word, length, 3);
        if (checkTrue == 3)
        {
            PutLetter(word, length, checkTrue, iTarget, jTarget);
            checkOutput++;
        }

        return checkTrue;
    }

    private int DownRightSide(int iTarget, int jTarget, int sqr, string[] word, int length, ref int checkOutput)
    {
        int checkTrue;
        checkTrue = CheckEmptyOrWhat2(iTarget + 1, jTarget + 1, sqr, word, length, 6);
        if (checkTrue == 6)
        {
            PutLetter(word, length, checkTrue, iTarget, jTarget);
            checkOutput++;
        }

        return checkTrue;
    }

    private int TopRightSide(int iTarget, int jTarget, int sqr, string[] word, int length, ref int checkOutput)
    {
        int checkTrue;
        checkTrue = CheckEmptyOrWhat2(iTarget - 1, jTarget + 1, sqr, word, length, 5);
        if (checkTrue == 5)
        {
            PutLetter(word, length, checkTrue, iTarget, jTarget);
            checkOutput++;
        }

        return checkTrue;
    }
    
    private void PutLetter(string[] word, int lenght, int checkTrue, int iTarget, int jTarget)
    {
        int sqr = floorSqrt(matrixLength + 1);
        for (int k = lenght - 1; k >= 0; k--)
        {
            switch (checkTrue)
            {
                case 1:
                    puzzelList[(iTarget - k) * sqr + jTarget].GetComponentInChildren<Text>().text = word[k];
                    break;
                case 2:
                    puzzelList[(iTarget + k) *sqr + jTarget].GetComponentInChildren<Text>().text = word[k];
                    break;
                case 3:
                    puzzelList[iTarget * sqr + (jTarget - k)].GetComponentInChildren<Text>().text = word[k];
                    break;
                case 4:
                    puzzelList[iTarget * sqr + (jTarget + k)].GetComponentInChildren<Text>().text = word[k];
                    break;
                case 5:
                    puzzelList[(iTarget - k) * sqr + (jTarget + k)].GetComponentInChildren<Text>().text = word[k];
                    break;
                case 6:
                    puzzelList[(iTarget + k) * sqr + (jTarget + k)].GetComponentInChildren<Text>().text = word[k];
                    break;
            }
        }
    }

    private int CheckEmptyOrWhat2(int iTarget, int jTarget, int sqr, string[] word, int length, int choice)
    {
        switch (choice)
        {
            case 1: //up
                if (length == 1)
                {
                    return 1;
                }

                if (puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text == ""
                    || puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text ==
                    word[word.Length - length + 1]) 
                {
                    return CheckEmptyOrWhat2(iTarget - 1, jTarget, sqr, word, length - 1, choice);
                }
                else
                {
                    return 0;
                }

            case 2: //down
                if (length == 1)
                    return 2;


                if (puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text == ""
                    || puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text ==
                    word[word.Length - length + 1])
                {
                    return CheckEmptyOrWhat2(iTarget + 1, jTarget, sqr, word, length - 1, choice);
                }
                else
                {
                    return 0;
                }

            case 3: //left

                if (length == 1)
                    return 3;

                if (puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text == ""
                    || puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text ==
                    word[word.Length - length + 1])
                {
                    return CheckEmptyOrWhat2(iTarget, jTarget - 1, sqr, word, length - 1, choice);
                }
                else
                {
                    return 0;
                }

            case 4: //right

                if (length == 1)
                    return 4;

                if (puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text == ""
                    || puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text ==
                    word[word.Length - length + 1])
                {
                    return CheckEmptyOrWhat2(iTarget, jTarget + 1, sqr, word, length - 1, choice);
                }
                else
                {
                    return 0;
                }

            case 5: //top - right
                if (length == 1)
                    return 5;

                if (puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text == ""
                    || puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text ==
                    word[word.Length - length + 1])
                {
                    return CheckEmptyOrWhat2(iTarget - 1, jTarget + 1, sqr, word, length - 1, choice);
                }
                else
                {
                    return 0;
                }

            case 6: //down - right
                if (length == 1)
                    return 6;

                if (puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text == ""
                    || puzzelList[iTarget * sqr + jTarget].GetComponentInChildren<Text>().text ==
                    word[word.Length - length + 1])
                {
                    return CheckEmptyOrWhat2(iTarget + 1, jTarget + 1, sqr, word, length - 1, choice);
                }
                else
                {
                    return 0;
                }

            default:
                return 0;
        }
    }
    
    public void FillWhitePlaces(int row)
    {
        for (int i = 0; i < row; i++)
            for (int j = 0; j < row; j++)
            {
                int rand = Random.Range(0, 32);
                if (puzzelList[i * row + j].GetComponentInChildren<Text>().text == "")
                    puzzelList[i * row + j].GetComponentInChildren<Text>().text = persianAlphabet[rand];
            }
    }
}
