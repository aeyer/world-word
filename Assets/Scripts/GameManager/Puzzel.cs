﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class Puzzel : MonoBehaviour , IPointerEnterHandler ,IPointerDownHandler
{
    private float  diffrenceXx , diffrenceYy, Radius ;

    private static Vector2 startPos;
    private Vector2 touchPos;
    private Vector2 finallPos;
    private Vector2 CheckPos;
    public Func<Vector2, Vector2, bool> SelectCells;
    private float distanceX;
    private float distanceY;


    public void OnPointerDown(PointerEventData eventData)
    {
        startPos = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (Input.GetMouseButton(0))
        {
            touchPos = new Vector2(gameObject.transform.position.x,gameObject.transform.position.y);
   
            FindFinallPosition();
        }else
        {
            Destroy(GameObject.Find("Line1"));
        }
            
    }

    void FindFinallPosition()
    {

        diffrenceXx = Mathf.Abs(touchPos.x - startPos.x);
        diffrenceYy = Mathf.Abs(touchPos.y - startPos.y);
        float alphaDeg = findArcTang(diffrenceXx, diffrenceYy);
        
        checkTrueWayForPoint(alphaDeg);
        
    }
    

    private void checkTrueWayForPoint(float alpha)
    {
        if (diffrenceXx == 0 && diffrenceYy == 0)
        {
            finallPos.x = startPos.x;
            finallPos.y = startPos.y;
        }
        else
        {
            if (alpha >= 0 && alpha <= 22.5)
            {
                finallPos.x = touchPos.x;
                finallPos.y = startPos.y;
            }
            else if (alpha > 22.5 && alpha < 67.5)
            {
                float littleOne;
                distanceX = touchPos.x - startPos.x;
                distanceY = touchPos.y - startPos.y;

                if (Mathf.Abs(distanceX) > Mathf.Abs(distanceY))
                    littleOne = distanceY;
                else
                    littleOne = distanceX;

                if (Mathf.Sign(distanceX) == Mathf.Sign(distanceY))
                {
                    finallPos.x = littleOne + startPos.x;
                    finallPos.y = littleOne + startPos.y;
                }
                else
                {
                    finallPos.x = startPos.x - Mathf.Sign(distanceY) * Mathf.Abs(littleOne);
                    finallPos.y = startPos.y + Mathf.Sign(distanceY) * Mathf.Abs(littleOne);
                }
            }
            else if (alpha >= 67.5 && alpha <= 90)
            {
                finallPos.x = startPos.x;
                finallPos.y = touchPos.y;
            }
        }
        
    //    finallPos.x = Mathf.RoundToInt(finallPos.x);
    //    finallPos.y = Mathf.RoundToInt(finallPos.y);
        

        SelectCells(finallPos, startPos);
        
    }
    
    
    float findArcTang(float x, float y)
    {
        float angleBoardXy = y / x;
        float angleBoardRot = Mathf.Rad2Deg*Mathf.Atan(angleBoardXy);

        return angleBoardRot;
    }
}


