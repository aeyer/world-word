﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatus : MonoBehaviour
{

    private string SelectString;
    private static GameStatus instance;
    private int hardShip ;
    void Start()
    {
        GameManager.SetNumber = SendHardShip;
        ManageTopMain.SendButtonNumber = SetHardShip;
        Cell.SelectMenu = SetSelectMenu;

        if (instance != null)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
        GameObject.DontDestroyOnLoad(this.gameObject);
        
        
        /////////

        SetText.GetString = SendString;
        SetText.SelectHardShip = SendHardShip;
    }

    public int SendHardShip()
    {
        return hardShip;
    }

    public bool SetHardShip(int num)
    {
        hardShip = num;
        return true;
    }

    public string SendString()
    {
        return SelectString;
    }
    public bool SetSelectMenu(string selecting)
    {
        SelectString = selecting;
        return true;
    }
}
