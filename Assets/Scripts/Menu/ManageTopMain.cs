﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ManageTopMain : MonoBehaviour
{
    private List<GameObject> ButtonList= new List<GameObject>();
    public GameObject index0;
    public GameObject index1;
    public GameObject index2;
    private int ButtonNumber = 0;
    private Color c = new Color(151, 111, 1);
    public static Func<int, bool> SendButtonNumber;


    private void Start()
    {
        SpawnButtonList(ButtonList);

        SendButtonNumber(ButtonNumber);
    }

    void SpawnButtonList(List<GameObject> Button)
    {
        ButtonList.Add(index0);
        ButtonList.Add(index1);
        ButtonList.Add(index2);
    }

    public void  OnClicked(Button button)
    {
        for (int i = 0; i < ButtonList.Count; i++)
        {
            ButtonList[i].GetComponentInChildren<Image>().color = Color.white;
            ButtonList[i].GetComponentInChildren<Text>().color = Color.black;
            
        }

        string str = button.name;
        int index =Int32.Parse(str);
        ButtonNumber = index;
        
        SendButtonNumber(ButtonNumber);
        
        button.GetComponentInChildren<Image>().color = new Color32(184,253,129,255);
        button.GetComponentInChildren<Text>().color = Color.white;
    }
}
