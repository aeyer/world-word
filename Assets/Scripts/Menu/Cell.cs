﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class Cell : MonoBehaviour ,IPointerDownHandler
{
    public string selectCell;
    public static Func<string, bool> SelectMenu;

    public void OnPointerDown(PointerEventData eventData)
    {
        selectCell = gameObject.name;
        SelectMenu(selectCell);

    }
}
