﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Debug = System.Diagnostics.Debug;

public class MainMenu : MonoBehaviour
{
    public void LoadScenes( string sceneName)
    {
        SceneManager.LoadScene( sceneName ); 
    }
    
}
